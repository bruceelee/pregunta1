# Se requiere instalar #

```
#!python

sudo apt-get install memcached
sudo pip install python-memcached
sudo pip install xmltodict
```


# Otras librerias utilizadas que debiesen ya estar: #
* urllib2
* json


Para utilizar se debe estar en la ruta /mysite y ejecutar
 
```
#!python

python manage.py runserver

```

# Observaciones # 

* Se debe verificar que la ruta 127.0.0.1:11211 este funcionando que corresponde al acceso a cache.
* El servicio response a localhost:8000


# Pendientes # 

Debes crear un proyecto utilizando python 2.7 y la ultima versión de Django el cual responda en el index o base (ej: http:/localhost/) con la siguiente estructura:


```
#!python

{"a": "b", "c": {"d": "e"}, "completed_in": "0.04229 segundos", "datetime": "2016-06-23T13:46:56.109427"}
```



Tener presente (consideraciones):

* ~~La estructura debe ser generada mediante un diccionario de datos con la utilización de cache de duración 5 segundos.~~

* ~~Si no existe cache, el servicio debe demorar su respuesta 2 segundos.~~

* Ocupando un decorador se debe agregar al diccionario de datos devuelto por la función la variable “datetime” con la hora actual.

* ~~La hora de la variable “datetime” debe ser formateada con norma ISO8601 con formato “YYYY-MM- DDThh:mm:ss.sss “ utilizando la zona horaria de Chile(-3).~~

*~~El tiempo en la variable *completed_in* debe contener el tiempo que demoro la respuesta del servicio con 4 decimales(sin cache). ~~