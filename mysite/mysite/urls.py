"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin

from django.core.cache import cache
from django.http import JsonResponse

import json

from datetime import datetime
from time import sleep
import time


def date_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError

def my_view(request):
	results = cache.get('dict')
	start_time = time.clock()
	
	#Si la cache no esta vacia solo la imprimo para comprobar
	if results is not None:
		print 'in cache '+ results
	#Se carga la cache 
   	else:
 		results =  '{ "c": { "d": "e"} , "a" : "b" }'
    	cache.set('dict', results, 5)
    	sleep(2)

	response= json.loads(results)
	completed_in = time.clock() - start_time
	response['completed_in']=  "{:.5f}".format(completed_in)  + ' segundos'
	response['datetime'] = json.dumps(datetime.now(), default=date_handler)
	return JsonResponse(response)

urlpatterns = [
	url(r'', my_view),
    url(r'^admin/', admin.site.urls),
]
